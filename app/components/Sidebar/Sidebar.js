import React from "react";
import {
  AppRegistry,
  Image,
  StatusBar,
  ImageBackground,
  StyleSheet
} from "react-native";
import {
  Container,
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Header,
  Body,
  Title
} from "native-base";

import theme from "../../assets/css/style";

const routes = [
  {
    title: "Accounts",
    icon: "home"
  },
  {
    title: "Profile",
    icon: "settings"
  },
  {
    title: "Downloads",
    icon: "information-circle"
  }
];

export default class SideBar extends React.Component {
  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Title>&nbsp;</Title>
          </Body>
        </Header>
        <Content>
          <List
            style={theme.lightBgColor}
            dataArray={routes}
            renderRow={data => {
              return (
                <ListItem
                  button
                  onPress={() => this.props.navigation.navigate(data.title)}
                >
                  <Icon active type="Ionicons" name={data.icon} />
                  <Text style={theme.marginLeft}>{data.title}</Text>
                </ListItem>
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}
