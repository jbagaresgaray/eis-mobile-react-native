"use strict";
import { StyleSheet } from "react-native";
import $colors from "./colors";

module.exports = StyleSheet.create({
  primaryColor: {
    color: $colors.primary
  },
  primaryBgColor: {
    backgroundColor: $colors.primary
  },
  secondaryColor: {
    color: $colors.secondary
  },
  secondaryBgColor: {
    color: $colors.secondary
  },
  dangerColor: {
    color: $colors.danger
  },
  dangerBgColor: {
    backgroundColor: $colors.danger
  },
  lightColor: {
    color: $colors.light
  },
  lightBgColor: {
    backgroundColor: $colors.light
  },
  darkColor: {
    color: $colors.dark
  },
  darkBgColor: {
    backgroundColor: $colors.dark
  },
  j3Color: {
    color: $colors.j3
  },
  j3BgColor: {
    backgroundColor: $colors.j3
  },
  j3secondaryColor: {
    color: $colors.j3secondary
  },
  j3secondaryBgColor: {
    backgroundColor: $colors.j3secondary
  },
  j3darkColor: {
    color: $colors.j3dark
  },
  j3darkBgColor: {
    backgroundColor: $colors.j3dark
  },
  j3navsColor: {
    color: $colors.j3navs
  },
  j3navsBgColor: {
    backgroundColor: $colors.j3navs
  },
  j3lightColor: {
    color: $colors.j3light
  },
  j3lightBgColor: {
    backgroundColor: $colors.j3light
  },
  j3warningColor: {
    color: $colors.j3warning
  },
  j3warningBgColor: {
    backgroundColor: $colors.j3warning
  },
  j3lightwarningColor: {
    color: $colors.j3lightwarning
  },
  j3lightwarningBgColor: {
    backgroundColor: $colors.j3lightwarning
  },
  j3noteColor: {
    color: $colors.j3note
  },
  j3noteBgColor: {
    backgroundColor: $colors.j3note
  }
});
