"use strict";

const $colors = {
  primary: "#488aff",
  secondary: "#32db64",
  danger: "#f53d3d",
  light: "#f4f4f4",
  dark: "#222",
  j3: "#003A8F",
  j3secondary: "#0EB260",
  j3dark: "#3C4044",
  j3navs: "#949CAE",
  j3light: "#F5F8FC",
  j3warning: "#F15C0F",
  j3lightwarning: "#FFF6F2",
  j3note: "#aeacb4"
};

export default $colors;
