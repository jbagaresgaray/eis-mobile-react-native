"use strict";

import { request } from "../actions/config";

import { encode as btoa } from "base-64";

function authenticate(data) {
  const authdata = btoa(data.username + ":" + data.password);
  return request({
    url: "login",
    method: "POST",
    headers: { Authorization: "Basic " + authdata }
  });
}

const AuthProvider = {
  authenticate
};

export default AuthProvider;
