import React from "react";
import { StatusBar, StyleSheet, Image } from "react-native";
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content,Text, Card, CardItem, View } from "native-base";

import styles from "../../assets/css/style";

export default class HomePage extends React.Component {
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}>
              <Icon style={styles.lightColor} name="menu" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.lightColor}>HomeScreen</Title>
          </Body>
          <Right>
          <Button
              transparent>
              <Icon style={styles.lightColor} name="settings" />
            </Button>
          </Right>
        </Header>
        <Content padder style={[styles.padding]}>
        <Text h4 style={[styles.textCenter, styles.lightColor]}>Choose the number of pulleys</Text>
        </Content>
      </Container>
    );
  }
}
