"use strict";
import { StyleSheet } from "react-native";

module.exports = StyleSheet.create({
  loginContent: {
    paddingTop: 100,
    paddingLeft: 30,
    paddingRight: 30,
    textAlign: "center",
    alignItems: "center",
    flex: 1,
    justifyContent: "center"
  },
  bgContent: {
    flex: 1,
    resizeMode: "cover",
    position: "absolute"
  },
  loginLogo: {
    width: 130,
    height: 130,
    resizeMode: "contain",
    marginTop: 80,
    marginBottom: 30
  },
  form: {
    width: "100%"
  }
});
