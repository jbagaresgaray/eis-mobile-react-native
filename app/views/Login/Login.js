import React from "react";
import { Image } from "react-native";
import {
  Container,
  Header,
  Left,
  Icon,
  Right,
  Button,
  Body,
  Content,
  Text,
  View,
  Title,
  Form,
  Item as FormItem,
  Label,
  Input,
  Toast
} from "native-base";
import Spinner from "react-native-loading-spinner-overlay";

import theme from "../../assets/css/style";
import styles from "./style";

import AuthProvider from "../../services/auth";
import { setLocalStorage } from "../../services/storage";
import { constants } from "../../actions/config";

export default class LoginPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      users: {
        username: "admin",
        password: "admin"
      },
      spinner: false
    };
  }

  loginApp() {
    console.log("loginApp");
    this.setState({
      spinner: true
    });
    const { username, password } = this.state.users;

    AuthProvider.authenticate({ username, password }).then(
      data => {
        if (data) {
          const resp = data.response;
          console.log("resp: ", resp);

          setLocalStorage(constants.APP_USER_TOKEN, resp.token);
          setLocalStorage(constants.APP_USER_INFO, JSON.stringify(resp.user));
          Toast.show({
            text: "Login successfully!",
            buttonText: "Okay",
            position: "bottom"
          });
        }
        this.setState({
          spinner: false
        });
        this.props.navigation.navigate("Drawer");
      },
      error => {
        console.log("error: ", error);
        Toast.show({
          text: JSON.stringify(error),
          buttonText: "Okay",
          position: "bottom"
        });
        // this.setState({
        //   spinner: false
        // });
      }
    );
  }

  render() {
    // const loginLogo = require("../../assets/img/logo.png");
    // const bgImage = require("../../assets/login.png");

    return (
      <Container>
        <Content>
          <Spinner
            visible={this.state.spinner}
            textContent={"Loading..."}
            textStyle={styles.spinnerTextStyle}
          />
          {/* <Image source={bgImage} style={styles.bgContent} /> */}
          <View style={styles.loginContent}>
            {/* <Image source={loginLogo} style={styles.loginLogo} /> */}
            <Form style={styles.form}>
              <FormItem floatingLabel style={theme.lightColor}>
                <Label style={theme.lightColor}>Email</Label>
                <Input
                  value={`${this.state.users.username}`}
                  onChangeText={inputValue => {
                    this.setState(prevState => ({
                      users: {
                        ...prevState.users,
                        username: inputValue
                      }
                    }));
                  }}
                />
              </FormItem>
              <FormItem floatingLabel last>
                <Label>Password</Label>
                <Input
                  secureTextEntry={true}
                  value={`${this.state.users.password}`}
                  onChangeText={inputValue => {
                    this.setState(prevState => ({
                      users: {
                        ...prevState.users,
                        password: inputValue
                      }
                    }));
                  }}
                />
              </FormItem>

              <Button
                full
                primary
                style={[theme.marginTop]}
                onPress={() => this.loginApp()}
              >
                <Text> Login </Text>
              </Button>
              <Button
                full
                primary
                style={[theme.marginTop]}
                onPress={() =>
                  Toast.show({
                    text: "Wrong password!",
                    buttonText: "Okay"
                  })
                }
              >
                <Text> Sign Up </Text>
              </Button>
            </Form>
          </View>
        </Content>
      </Container>
    );
  }
}
