import environment from "./environment";
import axios from "axios";

export const baseURL = environment.api_url + environment.api_version;
export const client = axios.create({
  baseURL: baseURL
});

/**
 * Request Wrapper with default success/error actions
 */
export const request = options => {
  const onSuccess = response => {
    console.log("Request Successful!", response);
    return response.data;
  };

  const onError = error => {
    console.log("Request Failed:", error.config);

    if (error.response) {
      // Request was made but server responded with something
      // other than 2xx
      console.log("Status:", error.response.status);
      console.log("Data:", error.response.data);
      console.log("Headers:", error.response.headers);
    } else {
      // Something else happened while setting up the request
      // triggered the error
      console.log("Error Message:", error.message);
    }

    return Promise.reject(error.response || error.message);
  };

  return client(options)
    .then(onSuccess)
    .catch(onError);
};

export const constants = {
  APP_USER_TOKEN: "app.eis.token",
  APP_USER_INFO: "app.eis.user",
  USERS: "users_table",
  CURRENT_USER: "app.eis.current_user",
  INSPECTIONS: "inspections",
  INSPECTION_DETAIL: "inspection_detail",
  EQUIPMENT_CHECKLIST: "equipment_checklist",
  CHECKLISTS: "checklists",
  CHECKLISTS_INPUT: "checklists_input",
  CHECKLISTS_INPUT_IMAGE: "checklists_input_image",
  CHECKLIST_DRAFT: "checklist_draft",
  SCANNED_INSPECTION: "scanned_inspection"
};
