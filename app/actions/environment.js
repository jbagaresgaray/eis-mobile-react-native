const environment = {
  production: false,
  // api_url: "http://178.128.102.191:5000",
  // api_url: "http://178.128.102.191:5001",
  api_url: "https://eis-production.herokuapp.com",
  api_version: "/mobile/api/1.0/",
  api_key: "",
  api_secret_key: "",
  googleWebClient: "",
  googleAndroidClient: "",
  REVERSED_CLIENT_ID: "",
  googleBrowserKey: "",
  facebookAppID: "",
  facebookAppSecret: "",
  httpPrefix: "http:",
  sendBird_API_ID: "",
  OneSignal_GoogleID: "",
  OneSignal_AppID: ""
};

export default environment;
