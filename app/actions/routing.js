import React from "react";
import { Root } from "native-base";
import {
  createDrawerNavigator,
  createStackNavigator,
  createAppContainer
} from "react-navigation";  

import LoginPage from "../views/Login/Login";
import HomePage from "../views/Home/Home";

import SideBar from "../components/Sidebar/Sidebar";

const MainDrawerNavigator = createAppContainer(
  createDrawerNavigator(
    {
      Home: {
        screen: HomePage
      }
    },
    {
      contentComponent: props => <SideBar {...props} />
    }
  )
);

const AppStack = createAppContainer(
  createStackNavigator(
    {
      Login: {
        screen: LoginPage,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      },
      Drawer: {
        screen: MainDrawerNavigator,
        navigationOptions: {
          header: null,
          gesturesEnabled: false
        }
      }
    },
    {
      initialRouteName: "Login",
      headerMode: "null"
    }
  )
);

export default () => (
  <Root>
    <AppStack />
  </Root>
);
